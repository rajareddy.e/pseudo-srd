# SRD pseudocode vscode extension and tools

1. Install python from Appstore.
   1. install python-docx from command prompt : [pip install python-docx]()
3. Install 'Microsoft visual studio code' from Appstore.
4. Launch vscode (Microsoft visual studio code)
5. Open vscode extensions tab and install below extensions :
   1. pseudo-pspec ,			#Needed
   2. vscode-icons ,				#Needed
   3. Custom Local Formatters ,	#Needed
   4. Hide Comments ,			#Optional
   5. Bracket Pair Colorizer 2 .		#Optional
   6. GitHub Copilot                        #Optional
6. Register custom pseudocode language formatter by updating settings.json.`

```
"customLocalFormatters.formatters": 
[
	{
	"command": "python %USERPROFILE%\\.vscode\\extensions\\pseudo-srd.pseudo-		0.1.5\\scripts\\format_pseudo_files.py",
	"languages": ["pseudo"]
	}
],
```
![](image/README/1639449476546.png)]()

1. open : %USERPROFILE%\\.vscode\extensions\pseudo-srd.pseudo-0.1.5\scripts
   1. copy "extract_all_pspecs.py" to "C:\srd"
   2. Copy SRD winword document (example : Rs_T_A400392-XC3_accepted.docx) to "C:\srd"
   3. run this command in cmd  : "python extract_all_pspecs.py Rs_T_A400392-XC3_accepted.docx"!!  
   ![](image/README/1639453326907.png)]()
2. Open "visual studio code" and select 'open folder' from file menu and browse 'accepted' folder.
3. With these steps you will have all the pseudo files in vscode with syntax highlghting with correct format.
4. Git is well integrated in vscode. Local changes will be listed in vscode source control tab If the opened forder / workspace is under version control.
