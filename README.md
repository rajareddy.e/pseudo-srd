# pseudo extension for SRD pseudocode

> SRD Pseudocode syntax highlighter and pseudocode custom formatter .

## Prerequisites

Install 'Visual Studio Code’ from Appstore

Install vscode extensions ‘pseudo-pspec',  'vscode-icons',  'Custom Local Formatters', 'Hide Comments' and 'Bracket Pair Colorizer 2'

Convert SRD docx to *.pseudo files using ‘[extract_all_pspecs.py]()’  (SRD.docx markups should be in accepted state).

For .pseudo files formatting use '[format_pseudo_files.py]()' and register this by updating vscode settings as mentioned in 'Custom Local Formatters'. This will enable vscode formatting for pseudo language (alf+shift+f) 

You can also open the Settings editor from the **Command Palette** (**Ctrl+Shift+P**) with **Preferences: Open Settings** [or]() use the keyboard shortcut (**Ctrl+,**).

**[For Windows :
]()**
add below configuration snippet in settings.json at the top just after the {
*"customLocalFormatters.formatters": [
{
"command": "python %USERPROFILE%\\.vscode\\extensions\\pseudo-srd.pseudo-0.1.5\\scripts\\format_pseudo_files.py",
"languages": ["pseudo"]
}
],*

Screen shot:

[
![](image/README/1639312044989.png)]()
**[Note:]()**  python scripts can be found in %USERPROFILE%\\.vscode\\extensions\\pseudo-srd.pseudo-0.1.5\scripts

**[For Linux :]()**
add below configuration snippet in settings.json at the top just after the {

*"customLocalFormatters.formatters": [
{
"command": "python3 $HOME/.vscode/extensions/pseudo-srd.pseudo-0.1.5/format_pseudo_files.py",
"languages": ["pseudo"]
}
],*
[Screen shot:]()

![img](image/README/1639309085314.png)

## Observation:

1. Windows search command has path length limitations while searching in files.
2. Linux vscode does not have file path limitations. Ubuntu vscode works without any limitations.

## Repository:

[https://gitlab.com/rajareddy.e/pseudo-srd.git ]()
